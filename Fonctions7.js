/*Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

Vous êtes un homme et vous êtes majeur
Vous êtes un homme et vous êtes mineur
Vous êtes une femme et vous êtes majeur
Vous êtes une femme et vous êtes mineur*/

function MorF(age,gender){
  if(age>18 && gender=="M"){
    console.log("Vous êtes un homme et vous êtes majeur");
  } else if (age<18 && gender=="M"){
    console.log("Vous êtes un homme et vous êtes mineur");
  }else if ((age>18 && gender=="F")){
    console.log("Vous êtes une femme et vous êtes majeur");
  }else{
    console.log("Vous êtes une femme et vous êtes mineur");
  }
}

