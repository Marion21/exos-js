/*Exercice 9 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.

*/
var departements = {  
  02 : "Aisne",
  59 : "Nord",
  60 : "Oise",
  62 : "Pas de Calais",
  80 : "Somme",
  };

for(var key in departements)
{
  var value = departements[key];
  console.log(key + " = " + value);
}